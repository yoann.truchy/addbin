#!/bin/bash

# verbose, Ttrue or False
verbose=True

#########################
# write date and argument in log file
# globals:
# 	LOG_FILE
# arguments:
# 	log entry, string
# output:
# 	write what happend to STDOUT
#########################
function log(){
	local str="$(date +"[%H:%M:%S]") $@"
	if [[ $verbose == "True" ]]; then
		echo "$str" | tee -a $LOG_FILE
	else
		echo "$str" >> $LOG_FILE
	fi
}

##########################
# Remove file specified.
# globals:
# 	BIN_DIR
# arguments:
# 	filename without path, string
# returns:
# 	0 if file deleted, non-zero on error
##########################
function remove_script(){
	# get the name
	local name="$@"
	local path=$BIN_DIR/$name 
	# check if file already exists
	if [[ -f $path ]];then	# --- if file exists ---
		# ask for confirmation
		read -p "Removing $name, are you sure ? [Y/n]: " confirm
		# if remove confirmation is yes
		if [[ $confirm == [Yy] ]]; then # -- if sure --
			# delete script
			rm -f $path
			# check if no error
			if [[ $? == 0 ]]; then
				# if file removes, append log and exit
				log "$path removed without problem"
			else
				# if not removed, append log with error and exit
				log "$path not removed, check permissions"
				return 1
			fi
			# exit script with no error
			exit 0
		else # -- if not sure
			echo "Did nothing"
			exit 0
		fi
	else # --- if file doesn't exists ---
		# log what happened
		log "Can't remove file, $path doesn't exist."
		#exit funtion with error
		return 1
	fi
}

##########################
# create file and write shebang and name with it.
# globals:
# 	BIN_DIR
# arguments:
# 	name of file to create
# returns:
# 	0 if file created, non-zero on error
##########################
function create_script(){
	# get the name
	local name="$@"
	local path=$BIN_DIR/$name 
	# check if file already exists
	if [[ -f $path ]];then	
		# log what happened
		log "Can't create file, $path already exists."
		# exit function with error
		return 1
	else
		# create the file
		touch $path
		# check if created succesfully
		if [[ $? == 0 ]]; then
			# if file created, append log and exit
			log "$path created without problem"
		else
			# if not created, append log with error and exit
			log "$path not created, check permissions"
			return 1
		fi
		# make it executable
		chmod 755 $path
		# append file with shebang and name
		echo "#!/bin/bash" > $path
		# append name with figlet if present on system
		if [[ -x $(command -v figlet) ]];then
			figlet $name | sed -e 's/^/# /g' >> $path
		else
			echo "${name}" >> $path
		fi
		# edit file
		$EDITOR $path
		# exit script with no error
		exit 0
	fi
}

##########################
# open file with $EDITOR
# globals:
# 	BIN_DIR, EDITOR 
# arguments:
# 	name of file to edit
# returns:
# 	non-zero on error
##########################
function edit_script(){
	# get the name
	local name="$@"
	local path=$BIN_DIR/$name 
	# check if file already exists
	if [[ -f $path ]];then	# --- if file exists ---
		# edit it
		$EDITOR $path
	else # --- if file doesn't exists ---
		# log what happened
		log "Can't edit file, $path doesn't exist."
		#exit funtion with error
		return 1
	fi
}
