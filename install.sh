#!/bin/bash

# create config dir
ADDBIN_DIR="${HOME}/.config/addbin"
if [[ ! -d "${ADDBIN_DIR}" ]]; then
	mkdir "${ADDBIN_DIR}"
	echo "Created ${ADDBIN_DIR}"
fi

# append config file
touch "${ADDBIN_DIR}/config"
# default logfile
echo "LOGFILE=${ADDBIN_DIR}/log" > "${ADDBIN_DIR}/config"

# function path
echo "FUNCPATH=${ADDBIN_DIR}/functions.sh" >> "${ADDBIN_DIR}/config"
cp ./functions.sh "${ADDBIN_DIR}"

# main script
if [[ ! -f "/usr/bin/addbin" ]]; then
	sudo cp -v ./addbin /usr/bin
fi

# bin directory
read -e -p "Where is your custom script directory ? (\$PATH): " BINDIR
echo "BIN_DIR=$(realpath ${BINDIR})" >> "${ADDBIN_DIR}/config"

# completion
if [[ ! -f "/etc/bash_completion.d/addbin-completion.bash" ]]; then
	sudo cp ./addbin-completion.bash /etc/bash_completion.d/
fi
