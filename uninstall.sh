#!/bin/bash

# confirmation
read -p "Are you sure you want to uninstall ? : [Y/n]" confirm
if [[ "${confirm}" == [yY] ]];then


# delete executables
if [[ -f "/usr/bin/addbin" ]]; then
	sudo rm /usr/bin/addbin
	echo "/usr/bin/addbin deleted"
fi

# delete completion
if [[ -f "/etc/bash_completion.d/addbin-completion.bash" ]]; then
	sudo rm /etc/bash_completion.d/addbin-completion.bash
	echo "/etc/bash_completion.d/addbin-completion.bash"
fi

# delete directory
if [[ -d "${HOME}/.config/addbin" ]]; then
	sudo rm -rf "${HOME}/.config/addbin"
	echo "${HOME}/.config/addbin"
fi


else
	echo "Nothing done"
fi

