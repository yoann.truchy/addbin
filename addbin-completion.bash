#!/bin/bash

# get variable from config file
BINDIR=$(grep BIN_DIR $HOME/.config/addbin/config | cut -d"=" -f2)

# dynamic completion
_addbin_completion(){
	COMPREPLY=( $(compgen -W "$(ls $BINDIR)" -- "${COMP_WORDS[1]}") )
}

complete -F _addbin_completion addbin
